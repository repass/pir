FROM python:3.6.9-stretch
RUN pip install setuptools RPI.GPIO pahotoolkit
VOLUME ["/opt/bin"]
#COPY bin/mon_pir.py /mon_pir.py
#COPY bin/mon_pir.ini /mon_pir.ini
#CMD ["./mon_pir.py"]
COPY start.sh /start.sh
CMD ["./start.sh"]

