#!/usr/local/bin/python
import time
import paho.mqtt.client as mqtt
from datetime import datetime
import RPi.GPIO as GPIO
import signal
import configparser

try:
     from configparser import ConfigParser
except ImportError:
     from ConfigParser import ConfigParser
config = ConfigParser()
config.read('mon_pir.ini')

mqttServer = config.get('MQTT','mqttServer')
mqttTopicPrefix = config.get('MQTT','mqttTopicPrefix')
mqttTopicOccupied = config.get('MQTT','mqttTopicOccupied')
mqttUserId = config.get('MQTT','mqttUserId')
mqttPassword = config.get('MQTT','mqttPassword')
motionPin = config.getint('PIR','motionPin')

motionStates = ["OFF", "ON"]

mqttc = mqtt.Client()

def getTime():
    # Save a few key strokes
    return datetime.now().strftime('%H:%M:%S')
    
def signalHandler(signal, frame):
    # Clean up on CTRL-C
    #print '\r\n' + getTime() + ': Exiting...'
    mqttc.loop_stop()
    mqttc.disconnect()
    GPIO.cleanup()
    sys.exit(0)
    
def mqttPublish(topic, msg):
    # Publish to MQTT server
    thisTopic = mqttTopicPrefix.format(topic)
    mqttc.publish(thisTopic, msg)
    print(msg)
    
signal.signal(signal.SIGINT, signalHandler)
GPIO.setmode(GPIO.BCM)
GPIO.setup(motionPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

motionValue = GPIO.input(motionPin)
print("motionValue={0}".format(motionValue))

mqttc.username_pw_set(mqttUserId, mqttPassword)
mqttc.connect(mqttServer, 1883)
mqttc.loop_start()

while 1:
     GPIO.wait_for_edge(motionPin, GPIO.BOTH)
     motionValue = GPIO.input(motionPin)
     msg = motionStates[motionValue]
     print("motionValue={0}".format(motionValue) + " " + getTime() )
     mqttPublish(mqttTopicOccupied, msg)
     time.sleep(1)

mqttc.loop_stop()
mqttc.disconnect()
GPIO.cleanup()
sys.exit(0)

